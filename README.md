Sudoku
======

C++17 solver for Sudoku-like puzzles. The following [variants](https://en.wikipedia.org/wiki/Sudoku#Variants) are currently supported:

- Sudoku
- Mini Sudoku
- Samurai Sudoku


Usage
-----

```bash
: sudoku <path> [variant]
```

If `path` is a single file, the program will try to parse it and solve it. Board files consist of whitespace-separated integer values starting from the top-left cell. 'Empty' cells must be included with value zero.

If `path` is a directory, the program will non-recursively iterate and process every file in it.

If `variant` is omitted, the program will try to deduce it from the file name, e.g.:

```bash
: sudoku ../boards/samurai_medium_1.txt
(assumed 'samurai' variant)
```

Note that if `path` is a directory and `variant`is provided, the program will use it for every file in the directory.
