cmake_minimum_required(VERSION 3.3)
project(sudoku LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

add_executable(${PROJECT_NAME}
	board.cpp
	cell.cpp
	main.cpp
	solver.cpp
	layouts/layout.cpp
	layouts/samurai.cpp
	)
