#include "solver.hpp"

std::optional<Board>
Solver::solve(Board board)
{
	do {
		board.reduce_options();
	}
	while (board.apply_single_options());

	if (!board.is_consistent())
		return {};

	if (!board.has_empty_cells())
		return board;

	for (const auto m : board.get_moves()) {
		board.move(m);
		if (auto b = solve(board); b)
			return b;
	}

	return {};
}
