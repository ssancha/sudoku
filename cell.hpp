#pragma once

#include <vector>

struct Cell
{
	using value_t = unsigned int;

	value_t val = 0;
	std::vector<value_t> options;

	Cell() = default;

	bool is_empty() const;

	void remove_option(value_t val);
	void clear_options();
};
