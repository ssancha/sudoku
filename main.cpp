#include "board.hpp"
#include "solver.hpp"
#include "layouts/mini.hpp"
#include "layouts/samurai.hpp"
#include "layouts/sudoku.hpp"
#include <chrono>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <memory>

using namespace std;
namespace fs = std::filesystem;

void
solve_file(fs::path path, optional<string_view> layout_name, bool quiet = false);

optional<unique_ptr<Layout>>
construct_layout(string_view name);


int
main(int argc, char **argv)
{
	if (argc < 2) {
		cout << "No input files\n";
		return -1;
	}

	fs::path user_path(argv[1]);
	if (!fs::exists(user_path)) {
		cerr << user_path << " does not exist\n";
		return -1;
	}

	optional<string_view> layout_name = nullopt;
	if (argc > 2)
		layout_name = argv[2];

	if (fs::is_directory(user_path)) {
		for(auto& p: fs::directory_iterator(user_path))
			solve_file(p, layout_name, true);
	}
	else
		solve_file(user_path, layout_name);

	return 0;
}

void
solve_file(fs::path path, optional<string_view> layout_name, bool quiet)
{
	ifstream is(path);
	if (!is.is_open())
	{
		cerr << "Could not open " << path << '\n';
		return;
	}

	string ln;
	if (layout_name)
		ln = *layout_name;
	else {
		auto file_name = fs::path(path).filename().string();
		ln = file_name.substr(0, file_name.find("_"));
	}

	auto layout = construct_layout(ln);
	if (!layout)
	{
		cerr << "Unknown layout '" << ln << "'\n";
		return;
	}

	cout << "Solving " << path << " [" << ln << "]..." << endl;

	Board board(**layout);

	try {
		is >> board;
	}
	catch(...) {
		cerr << "Parse error\n";
		return;
	}

	if (!quiet)
		cout << board << endl;

	auto start = chrono::steady_clock::now();
	auto solved = Solver{}.solve(board);
	auto end = chrono::steady_clock::now();

	if (!quiet) {
		if (solved)
			cout << *solved;
		else
			cout << "Board cannot be solved";
		cout << endl;
	}

	cout << "Elapsed time: " 
		<< chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms\n";
}

optional<unique_ptr<Layout>>
construct_layout(string_view name)
{
	if (name == "sudoku")
		return make_unique<Sudoku>();
	else if (name == "mini")
		return make_unique<Mini>();
	else if (name == "samurai")
		return make_unique<Samurai>();
	else
		return {};
}
