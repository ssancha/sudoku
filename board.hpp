#pragma once

#include "cell.hpp"
#include "layout.hpp"
#include <istream>
#include <ostream>
#include <vector>

class Board
{
public:
	Board(const Layout& layout);

	bool has_empty_cells() const;
	bool is_consistent() const;

	void reduce_options();
	bool apply_single_options();

	struct move_t
	{
		size_t index;
		Cell::value_t val;
	};

	using move_list_t = std::vector<move_t>;
	move_list_t get_moves() const;

	void move(move_t m);

private:
	const Layout& layout;
	Layout::cell_list_t cells;

	bool group_has_duplicates(const Layout::group_t& group) const;
	bool group_has_value(const Layout::group_t& group, Cell::value_t val, size_t skip) const;

	void clear_occupied_options();
	void reduce_options_in_group(const Layout::group_t& group);
	void remove_option_in_group(const Layout::group_t& group, Cell::value_t val);

	friend std::istream& operator>>(std::istream& is, Board& b);
	friend std::ostream& operator<<(std::ostream& os, const Board& b);
};
