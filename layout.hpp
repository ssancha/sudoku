#pragma once

#include "cell.hpp"
#include <ostream>
#include <vector>

class Layout
{
public:
	using cell_list_t = std::vector<Cell>;

	using group_t = std::vector<size_t>;
	std::vector<group_t> groups;

	virtual size_t n_cells() const = 0;
	virtual size_t n_values() const = 0;

	virtual void print(std::ostream& os, const cell_list_t& cells) const = 0;

protected:
	struct grid_t
	{
		size_t start = 0;
		size_t rows = 9, cols = 9;
		size_t block_rows = 3, block_cols = 3;
		size_t stride = 9;
		group_t offset = group_t(cols, 0);
	};

	void add_grid(grid_t g);
	void print_grid(std::ostream& os, const cell_list_t& cells, grid_t g) const;
};
