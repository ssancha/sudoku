#pragma once

#include "layout.hpp"

class Samurai : public Layout
{
public:
	Samurai();

	size_t n_cells() const override
	{
		return grid.rows * grid.cols * n_grids -
			grid.block_rows * grid.block_rows * (n_grids - 1);
	};
	size_t n_values() const override { return grid.block_rows * grid.block_cols; }

	void print(std::ostream& os, const cell_list_t& cells) const override;

private:
	const grid_t grid;
	const int n_grids = 5;
};
