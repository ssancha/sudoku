#include "samurai.hpp"

using namespace std;

Samurai::Samurai()
{
	add_grid(grid_t{ .start = 0, .stride = 18, .offset = { 0, 0, 0, 0, 0, 0, 0, 3, 6 } });
	add_grid(grid_t{ .start = 9, .stride = 18, .offset = { 0, 0, 0, 0, 0, 0, 3, 6, 9 } });
	add_grid(grid_t{ .start = 114, .stride = 9, .offset = { 0, 12, 24, 30, 30, 30, 36, 48, 60 } });
	add_grid(grid_t{ .start = 198, .stride = 18, .offset = { 0, 3, 6, 9, 9, 9, 9, 9, 9 } });
	add_grid(grid_t{ .start = 210, .stride = 18, .offset = { 0, 3, 6, 6, 6, 6, 6, 6, 6 } });
}

void
Samurai::print(std::ostream& os, const cell_list_t& cells) const
{
	int col = 0, row = 0;
	for (const auto& c : cells) {
		bool new_line = false;
		if (row < 6 || row > 14) {
			if (col == 9)
				os << string(7, ' ');
			new_line = (col == 17);
		}
		else if (row < 9 || row > 11)
			new_line = (col == 20);
		else {
			if (col == 0)
				os << string(14, ' ');
			new_line = (col == 8);
		}

		os << c.val <<  ' ';
		if (col % grid.block_cols == grid.block_cols - 1)
			os << ' ';

		++col;

		if (new_line) {
			os << '\n';
			if (row % grid.block_rows == grid.block_rows - 1)
				os << '\n';
			col = 0;
			++row;
		}
	}
}

