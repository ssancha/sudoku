#include "layout.hpp"
#include <algorithm>
#include <numeric>

using namespace std;

void
Layout::add_grid(Layout::grid_t g)
{
	vector<group_t> rows(g.rows);

	size_t i = 0;
	for (auto& row : rows) {
		row.resize(g.cols);
		iota(begin(row), end(row), i * g.stride + g.start + g.offset[i]);
		++i;

		groups.push_back(row);
	}

	for (size_t i = 0; i < g.cols; ++i) {
		group_t col;
		for (const auto& row : rows)
			col.push_back(row[i]);

		groups.push_back(col);
	}

	for (size_t i = 0; i < g.cols / g.block_cols; ++i) {
		for (size_t j = 0; j < g.rows / g.block_rows; ++j) {
			group_t block;

			for (size_t ii = 0; ii < g.block_cols; ++ii)
				for (size_t jj = 0; jj < g.block_rows; ++jj)
					block.push_back(rows[jj + j * g.block_rows][ii + i * g.block_cols]);

			groups.push_back(block);
		}
	}
}

void
Layout::print_grid(std::ostream& os, const cell_list_t& cells, grid_t g) const
{
	string sep((g.cols + (g.cols / g.block_cols)) * 2 + 1, '-');
	os << sep << '\n';

	int col = 0, row = 0;
	for (const auto& c : cells) {
		if (col % g.block_cols == 0)
			os << "| ";

		if (c.is_empty())
			os << ". ";
		else
			os << c.val <<  ' ';

		if (++col == g.cols) {
			os << "|\n";
			if (row % g.block_rows == g.block_rows - 1)
				os << sep << '\n';
			col = 0;
			++row;
		}
	}
}
