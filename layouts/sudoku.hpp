#pragma once

#include "layout.hpp"

class Sudoku : public Layout
{
public:
	Sudoku() { add_grid(grid); }

	void print(std::ostream& os, const cell_list_t& cells) const override
	{ print_grid(os, cells, grid); }

	size_t n_cells() const override { return grid.rows * grid.cols; }
	size_t n_values() const override { return grid.block_rows * grid.block_cols; }

private:
	grid_t grid;
};
