#pragma once

#include "layout.hpp"

class Mini : public Layout
{
public:
	Mini() { add_grid(grid); }

	void print(std::ostream& os, const cell_list_t& cells) const override
	{ print_grid(os, cells, grid); }

	size_t n_cells() const override { return grid.rows * grid.cols; }
	size_t n_values() const override { return grid.block_rows * grid.block_cols; }

private:
	const grid_t grid{ .rows = 6, .cols = 6, .block_rows = 2, .block_cols = 3, .stride = 6 };
};
