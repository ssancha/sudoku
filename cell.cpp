#include "cell.hpp"
#include <algorithm>

bool
Cell::is_empty() const
{
	return val == 0;
}

void
Cell::remove_option(value_t val)
{
	options.erase(std::remove(options.begin(), options.end(), val), options.end());
}

void
Cell::clear_options()
{
	options.clear();
}
