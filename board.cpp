#include "board.hpp"
#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>

using namespace std;

Board::Board(const Layout& layout) : layout(layout)
{
	cells.resize(layout.n_cells());

	for (auto& c : cells) {
		c.options.resize(layout.n_values());
		iota(begin(c.options), end(c.options), 1);
	}
}

bool
Board::has_empty_cells() const
{
	return any_of(begin(cells), end(cells), [](const auto& c) {
		return c.is_empty();
	});
}

bool
Board::is_consistent() const
{
	return all_of(begin(layout.groups), end(layout.groups), [&](const auto& g) {
		return !group_has_duplicates(g);
	});

	return true;
}

bool
Board::group_has_duplicates(const Layout::group_t& group) const
{
	return any_of(begin(group), end(group), [&](const auto i) {
		return !cells[i].is_empty() && group_has_value(group, cells[i].val, i);
	});
}

bool
Board::group_has_value(const Layout::group_t& group, Cell::value_t val, size_t skip) const
{
	return any_of(begin(group), end(group), [&](const auto i) {
		return i != skip && cells[i].val == val;
	});
}

void
Board::reduce_options()
{
	clear_occupied_options ();

	for (const auto& g : layout.groups) 
		reduce_options_in_group(g);
}

void
Board::clear_occupied_options()
{
	for (auto& c : cells) {
		if (!c.is_empty())
			c.clear_options();
	}
}

void
Board::reduce_options_in_group(const Layout::group_t& group)
{
	for (const auto i : group) {
		if (!cells[i].is_empty())
			remove_option_in_group(group, cells[i].val);
	}
}

void
Board::remove_option_in_group(const Layout::group_t& group, Cell::value_t val)
{
	for (const auto i : group)
		cells[i].remove_option(val);
}

bool
Board::apply_single_options()
{
	bool applied = false;
	for (auto& c : cells) {
		if (c.is_empty() && c.options.size() == 1) {
			c.val = c.options[0];
			applied = true;
		}
	}

	return applied;
}

Board::move_list_t
Board::get_moves() const
{
	Board::move_list_t moves;
	size_t index = 0;

	for (auto& c : cells) {
		if (c.is_empty()) {
			for (const auto& o : c.options)
				moves.push_back({ index, o });
		}
		++index;
	}

	return moves;
}

void
Board::move(Board::move_t m)
{
	cells[m.index].val = m.val;
}

istream&
operator>>(istream& is, Board& b)
{
	string s;

	for (auto& c : b.cells) {
		is >> s;
		if (!is)
			break;

		c.val = stoi(s);
	}

	return is;
}

ostream&
operator<<(ostream& os, const Board& b)
{
	b.layout.print(os, b.cells);

	return os;
}
