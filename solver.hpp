#pragma once

#include "board.hpp"
#include <optional>

struct Solver
{
	Solver() = default;

	std::optional<Board> solve(Board board);
};
